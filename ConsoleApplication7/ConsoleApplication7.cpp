// ConsoleApplication7.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include<iostream>
#include <ctime>;
using namespace std;

void quickSort(int *left, int *right)
{
	int p = *left;
	int *l_hold = left;
	int *r_hold = right;
	while (left < right)
	{
		while ((*right >= p) && (left < right))
			right--;
		if (left != right)
		{
			*left = *right;
			left++;
		}
		while ((*left <= p) && (left < right))
			left++;
		if (left != right)
		{
			*right = *left;
			right--;
		}
	}
	*left = p;
	int *q = left;
	left = l_hold;
	right = r_hold;
	if (left < q)
		quickSort(left, q + 1);
	if (right > q)
		quickSort(q + 1, right);
}

int main()
{
	srand(time(0));
	int *a = new int[10],
		*left = &a[0],
		*right = &a[9];
	for (unsigned i = 0; i <10; ++i) {
		a[i] = rand() % 30 - 15;
		cout << a[i] << " ";
	}
	cout << "\n";

	quickSort(left, right);
	for (unsigned i = 0; i < 10; ++i)
		cout << a[i] << " ";
	cout << "\n";

	delete[] a;
	system("pause");
	return 0;
}